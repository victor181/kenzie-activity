#!/usr/bin/env node
// npm uninstall -g .; npm install -g .; kenzie-academy
const fs = require('fs')
const { exec } = require('child_process')
const buildYAMLDoc = require('./lib/build-yaml-doc')

exec("git remote -v", (error, stdout, stderr) => { 
  // TODO: All of this needs to be in a service or a class.
  const boldEscapeSequence = '\x1b[1m'
  const linkEscapeSequence = '\x1b[1;4;40;33m'
  const endEscapeSequence = '\x1b[0m'
  const boldWrap = string => `${boldEscapeSequence}${string}${endEscapeSequence}`
  const linkWrap = string => {
    const [, link, trailingWhitespace] = string.match(/^(.+?)(\s+?)$/) || []
    return `${linkEscapeSequence}${link || string}${endEscapeSequence}${trailingWhitespace || ''}`
  }
  const hashWrap = string => `# ${string} #`
  const prependNewLine = string => `\n${string}`
  const pad = (string, lineLength) => string.padEnd(lineLength, ' ')
  
  const [, gitLabUserName, gitLabProjectPath] = stdout.match(/gitlab\.com[:\/](.+?)\/(.+?)\.git\s+\(fetch\)$/m) || []

  if (!gitLabUserName || stderr) {
    console.error(stderr || 'fatal: Este repositório não possui um Repositório Gitlab remoto.\n')
    process.exit(1)
  }

  const gitLabPageURL = `https://${gitLabUserName}.gitlab.io/${gitLabProjectPath}`
  const message = 'Depois de ter dado push e merge, a URL do seu GitLab Page URL será:'
  const lineLength = gitLabPageURL.length >= message.length 
    ? gitLabPageURL.length 
    : message.length

  const horizontalRule = prependNewLine('#'.repeat(lineLength + 4))
  const messageLine = prependNewLine(hashWrap(boldWrap(pad(message, lineLength))))
  const urlLine = prependNewLine(hashWrap(linkWrap(pad(gitLabPageURL, lineLength))))

  console.log(`\n${horizontalRule}${messageLine}${urlLine}${horizontalRule}\n`)

  const [, , path = process.cwd()] = process.argv
  const publicExists = fs.existsSync(path + '/public')
  const publicIsEmpty = publicExists && !(fs.readdirSync(path + '/public').length)
  
  if (publicExists && publicIsEmpty) {
    console.warn(
      `WARNING: A pasta "public" desse projeto está atualmente vazia.\n Por favor, adicione algum conteúdo ao qual seja possível criar o server (como um arquivo "index.html")`
    )
  }
  
  const YAMLDoc = buildYAMLDoc(publicExists)
  
  console.log(
    `Escrevendo as seguintes configurações para "${path + '/.gitlab-ci.yml'}":\n${YAMLDoc}`
  )
  
  const writeStream = fs.createWriteStream(path + '/.gitlab-ci.yml')
  writeStream.write(YAMLDoc)
  writeStream.end()
})


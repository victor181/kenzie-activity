module.exports = (items, indentationLevel = 2) => 
  items.reduce(
    (collection, item) => {
      const indentation = "- ".padStart(indentationLevel * 2 + 2)
      return `${collection}\n${indentation}${item}`
    },
    ''
  )

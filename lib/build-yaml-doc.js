const createYAMLCollection = require('./create-yaml-collection')

module.exports = publicExists => {
  const shellCommands = {
    ifPublicExists: [
      `echo "Usando a pasta \\"public\\" do projeto"`
    ],
    ifPublicDoesNotExist: [
      `echo "Criando \\(e então usando\\) a nova pasta \\"public\\" dos arquivos da pasta raiz do projeto."`,
      `mkdir ./.public`,
      `cp -r * ./.public`,
      `mv ./.public ./public`
    ]
  }

  const deploymentShellCommands = publicExists
    ? createYAMLCollection(shellCommands.ifPublicExists)
    : createYAMLCollection(shellCommands.ifPublicDoesNotExist)

  return (
`pages:
  stage: deploy
  script: ${deploymentShellCommands}
  artifacts:
    paths:
      - public
  only:
    - main`
  )
}
